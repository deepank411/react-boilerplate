/* @flow */

import type { Store as ReduxStore } from 'redux';

export type Home = {
	readyStatus: string,
	err: any,
	list: Array<Object>,
};

export type Popular = {
	readyStatus: string,
	err: any,
	list: Array<Object>,
};

export type ReposLang = {
	readyStatus: string,
	fetching: boolean,
	fetched: boolean,
	error: any,
	repos: Array<Object>,
};

export type UserInfo = {
	[userId: string]: {
		readyStatus: string,
		err: any,
		info: Object,
	},
};

export type Reducer = {
	home: Home,
	popular: Popular,
	reposlang: ReposLang,
	userInfo: UserInfo,
	router: any,
};

export type Action =
{ type: 'USERS_REQUESTING' } |
{ type: 'USERS_SUCCESS', data: Array<Object> } |
{ type: 'USERS_FAILURE', err: any } |

{ type: 'REPOS_REQUESTING' } |
{ type: 'REPOS_SUCCESS', data: Array<Object> } |
{ type: 'REPOS_FAILURE', err: any } |

{ type: 'REPOS_NEW_REQUESTING' } |
{ type: 'REPOS_NEW_SUCCESS', repos: Array<Object> } |
{ type: 'REPOS_NEW_FAILURE', error: any } |

{ type: 'USER_REQUESTING', userId: string } |
{ type: 'USER_SUCCESS', userId: string, data: Object } |
{ type: 'USER_FAILURE', userId: string, err: any };

export type Store = ReduxStore<Reducer, Action>;
// eslint-disable-next-line no-use-before-define
export type Dispatch = (action: Action | ThunkAction | PromiseAction | Array<Action>) => any;
export type GetState = () => Object;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
