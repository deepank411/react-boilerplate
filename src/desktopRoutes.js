/* @flow */
/* eslint-disable import/no-named-as-default */

import type { Dispatch } from './types';
import { fetchReposIfNeeded } from './containers/desktop/PopularRepos/action';
import { fetchReposByLang } from './containers/desktop/ReposLang/action';
import HomePage from './containers/desktop/Home';
import ReposLang from './containers/desktop/ReposLang';
import PopularRepos from './containers/desktop/PopularRepos';

export default [
	{
		path: '/',
		exact: true,
		component: HomePage,
	},
	{
		path: '/repos-lang',
		component: ReposLang,
		loadData: (dispatch: Dispatch) => Promise.all([
			dispatch(fetchReposByLang()),
		]),
	},
	{
		path: '/popular',
		component: PopularRepos,
		loadData: (dispatch: Dispatch) => Promise.all([
			dispatch(fetchReposIfNeeded()),
		]),
	},
	// {
	// 	path: '*',
	// 	component: NotFoundPage,
	// },
];
