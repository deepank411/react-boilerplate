/* @flow */

import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter } from 'react-router-redux';

import configureStore from './redux/store';

import MobileDetect from 'mobile-detect';

let isMobile = false;
let md = new MobileDetect(window.navigator.userAgent);
if (md.mobile() && !md.tablet()) {
	isMobile = true;
}

// Get initial state from server-side rendering
const initialState = window.__INITIAL_STATE__;
const history = createHistory();
const store = configureStore(history, initialState);
const mountNode = document.getElementById('react-view');
let renderApp;

if(isMobile){
	renderApp = () => {
		const MobileApp = require('./containers/mobile/MobileApp').default;

		render(
			<AppContainer>
				<Provider store={store}>
					<ConnectedRouter history={history}>
						<MobileApp />
					</ConnectedRouter>
				</Provider>
			</AppContainer>,
			mountNode,
		);
	};
}
else{
	renderApp = () => {
		const DesktopApp = require('./containers/desktop/DesktopApp').default;

		render(
			<AppContainer>
				<Provider store={store}>
					<ConnectedRouter history={history}>
						<DesktopApp />
					</ConnectedRouter>
				</Provider>
			</AppContainer>,
			mountNode,
		);
	};
}

// Enable hot reload by react-hot-loader
if (module.hot) {
	const reRenderApp = () => {
		try {
			renderApp();
		} catch (error) {
			const RedBox = require('redbox-react').default;

			render(<RedBox error={error} />, mountNode);
		}
	};

	if(isMobile){
		module.hot.accept('./containers/mobile/MobileApp', () => {
			setImmediate(() => {
				// Preventing the hot reloading error from react-router
				unmountComponentAtNode(mountNode);
				reRenderApp();
			});
		});
	}
	else{
		module.hot.accept('./containers/desktop/DesktopApp', () => {
			setImmediate(() => {
				// Preventing the hot reloading error from react-router
				unmountComponentAtNode(mountNode);
				reRenderApp();
			});
		});
	}
}

renderApp();
