import React from 'react';
import './styles.scss';

type Props = { list: Object };

const Popular = ({ list }: Props) => (
	<div>
		<h2 className="title">Popular Repos Mobile</h2>

		<ul className="Popular">
			{list.map((repo, index) => (
				<li key={repo.name} className="popular_item">
					<div className="popular_rank">#{index + 1}</div>
					<ul className="space_list_items">
						<li>
							<img
								className="avatar"
								src={repo.owner.avatar_url}
								alt={`Avatar for ${repo.owner.login}`}
							/>
						</li>
						<li><a href={repo.html_url} target="_blank">{repo.name}</a></li>
						<li>@{repo.owner.login}</li>
						<li>{repo.stargazers_count} stars</li>
					</ul>
				</li>
			))}
		</ul>
	</div>
);

export default Popular;
