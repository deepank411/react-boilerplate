/* @flow */

import { routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import chalk from 'chalk';
import MobileDetect from 'mobile-detect';

import type { Store } from '../types';
import desktopRootReducer from './desktopReducers';
import mobileRootReducer from './mobileReducers';

let isMobile = false;
if (typeof window === 'object') {
	const md = new MobileDetect(window.navigator.userAgent);
	if (md.mobile() && !md.tablet()) {
		isMobile = true;
	}
}

export default (history: Object, initialState: Object = {}): Store => {
	const middlewares = [
		thunk.withExtraArgument(axios),
		routerMiddleware(history),
	];

	const enhancers = [
		applyMiddleware(...middlewares),
		__DEV__ && typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ?
		window.devToolsExtension() : f => f,
	];

	if (isMobile) {
		const store: Store = createStore(mobileRootReducer, initialState, compose(...enhancers));

		if (module.hot) {
			// Enable Webpack hot module replacement for reducers
			module.hot.accept('./mobileReducers', () => {
				try {
					const nextReducer = require('./mobileReducers').default;

					store.replaceReducer(nextReducer);
				} catch (error) {
					console.error(chalk.red(`==> 😭  Reducer hot reloading error ${error}`));
				}
			});
		}
		return store;
	} else {
		const store: Store = createStore(desktopRootReducer, initialState, compose(...enhancers));

		if (module.hot) {
			// Enable Webpack hot module replacement for reducers
			module.hot.accept('./desktopReducers', () => {
				try {
					const nextReducer = require('./desktopReducers').default;
					store.replaceReducer(nextReducer);
				} catch (error) {
					console.error(chalk.red(`==> 😭  Reducer hot reloading error ${error}`));
				}
			});
		}
		return store;
	}
};
