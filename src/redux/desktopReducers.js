/* @flow */
/* eslint-disable */

import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import reposlang from '../containers/desktop/ReposLang/reducer';
import popular from '../containers/desktop/PopularRepos/reducer';

export default combineReducers({
	reposlang,
	popular,
});
