/* @flow */
/* eslint-disable */

import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import reposlang from '../containers/mobile/ReposLang/reducer';
import popular from '../containers/mobile/PopularRepos/reducer';

export default combineReducers({
	reposlang,
	popular,
});
