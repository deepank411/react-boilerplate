/* @flow */

import React from 'react';
import { Route, Switch, Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import _ from 'lodash';

import config from '../../../config';
import routes from '../../../mobileRoutes';
// Import your global styles here
import '../../../theme/normalize.css';
import './styles.scss';

export default () => {
	// Use it when sub routes are added to any route it'll work
	const routeWithSubRoutes = route => (
		<Route
			key={_.uniqueId()}
			exact={route.exact || false}
			path={route.path}
			render={props => (
				// Pass the sub-routes down to keep nesting
				<route.component {...props} routes={route.routes} />
			)}
		/>
	);

	return (
		<div className="App">
			<Helmet {...config.app} />
			<div className="header">
				<img src={require('./assets/logo.svg')} alt="Logo" role="presentation" />
				<Link to={'/'}>
					<h4>{config.app.title}</h4>
				</Link>
				<Link to={'/popular'}>
					<h4>Popular</h4>
				</Link>
				<Link to={'/repos-lang'}>
					<h4>Repos by language</h4>
				</Link>
			</div>
			<hr />
			<Switch>
				{routes.map(route => routeWithSubRoutes(route))}
			</Switch>
		</div>
	);
};
