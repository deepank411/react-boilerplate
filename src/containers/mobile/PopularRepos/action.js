/* @flow */

import type {
	Dispatch,
	GetState,
	ThunkAction,
	Reducer,
} from '../../../types';

export const REPOS_INVALID = 'REPOS_INVALID';
export const REPOS_REQUESTING = 'REPOS_REQUESTING';
export const REPOS_FAILURE = 'REPOS_FAILURE';
export const REPOS_SUCCESS = 'REPOS_SUCCESS';

export const API_URL = 'https://api.github.com/search/repositories?q=stars:>1+language:python&sort=stars&order=desc&type=Repositories';

export const fetchRepos = (axios: any, URL: string = API_URL): ThunkAction =>
(dispatch: Dispatch) => {
	dispatch({ type: REPOS_REQUESTING });

	return axios.get(URL)
	.then((res) => {
		dispatch({ type: REPOS_SUCCESS, data: res.data });
	})
	.catch((err) => {
		dispatch({ type: REPOS_FAILURE, err });
	});
};

// Preventing double fetching data
/* istanbul ignore next */
const shouldFetchRepos = (state: Reducer): boolean => {
	// In development, we will allow action dispatching
	// or your reducer hot reloading won't updated on the view
	if (__DEV__) return true;

	const popular = state.popular;

	if (popular.readyStatus === REPOS_SUCCESS) return false; // Preventing double fetching data

	return true;
};

/* istanbul ignore next */
export const fetchReposIfNeeded = (): ThunkAction =>
(dispatch: Dispatch, getState: GetState, axios: any) => {
	/* istanbul ignore next */
	if (shouldFetchRepos(getState())) {
		/* istanbul ignore next */
		return dispatch(fetchRepos(axios));
	}

	/* istanbul ignore next */
	return null;
};
