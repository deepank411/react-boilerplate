/* eslint-disable react/sort-comp */
/* @flow */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';
import Helmet from 'react-helmet';

import * as action from './action';
import type { Popular as PopularType, Dispatch, Reducer } from '../../../types';

import Popular from '../../../components/mobile/Popular';
import Loading from '../Loading';
// import './styles.scss';

type Props = {
	popular: PopularType,
	fetchReposIfNeeded: () => void,
};

export class PopularRepos extends PureComponent {
	props: Props;

	static defaultProps: {
		popular: {
			readyStatus: 'REPOS_INVALID',
			list: null,
		},
		fetchReposIfNeeded: () => {},
	};

	componentDidMount() {
		this.props.fetchReposIfNeeded();
	}

	renderRepoList = () => {
		const { popular } = this.props;
		if (!popular.readyStatus || popular.readyStatus === action.REPOS_INVALID
			|| popular.readyStatus === action.REPOS_REQUESTING) {
			return <Loading />;
		}

		if (popular.readyStatus === action.REPOS_FAILURE) {
			return <p>Oops, Failed to load list!</p>;
		}

		return <Popular list={popular.info.items} />;
	}

	render() {
		return (
			<div>
				<Helmet>
					<title>Popular Repos title</title>
					<meta name="description" content="updated description" />
				</Helmet>
				{this.renderRepoList()}
			</div>
		);
	}
}

const connector: Connector<{}, Props> = connect(
	({ popular }: Reducer) => ({ popular }),
	(dispatch: Dispatch) => ({
		fetchReposIfNeeded: () => dispatch(action.fetchReposIfNeeded()),
	}),
);

export default connector(PopularRepos);
