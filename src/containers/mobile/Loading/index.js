import React from 'react';

import './styles.scss';

function Loading() {
	return (
		<div className="Loading">
			<p>Loading</p>
		</div>
	);
}

export default Loading;
