/* eslint-disable */

import React, { PureComponent } from 'react';

// import { connect } from 'react-redux';
// import type { Connector } from 'react-redux';
import Helmet from 'react-helmet';

export class Home extends PureComponent {

	render() {
		return (
			<div>
				<Helmet>
					<title>first mobile component</title>
					<meta data-react-helmet="true" name="description" content="mobileasa home page." />
				</Helmet>
				<p>mobile home test page 2</p>
			</div>
		);
	}
}

export default Home;
