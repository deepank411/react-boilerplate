/* @flow */

import _ from 'lodash';

import {
	REPOS_NEW_INVALID,
	REPOS_NEW_REQUESTING,
	REPOS_NEW_FAILURE,
	REPOS_NEW_SUCCESS,
} from './action';
import type { ReposLang, Action } from '../../../types';

type State = ReposLang;

const initialState = {
	readyStatus: REPOS_NEW_INVALID,
	error: null,
	repos: [],
	fetching: false,
	fetched: false,
};

export default (state: State = initialState, action: Action): State => {
	switch (action.type) {
	case REPOS_NEW_REQUESTING:
		return _.assign({}, state, {
			readyStatus: REPOS_NEW_REQUESTING,
			fetching: true,
		});
	case REPOS_NEW_FAILURE:
		return _.assign({}, state, {
			readyStatus: REPOS_NEW_FAILURE,
			fetching: false,
			fetched: false,
			error: action.payload,
		});
	case REPOS_NEW_SUCCESS:
		return _.assign({}, state, {
			readyStatus: REPOS_NEW_SUCCESS,
			fetching: false,
			fetched: true,
			repos: action.payload,
		});
	default:
		return state;
	}
};
