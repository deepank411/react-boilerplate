/* eslint-disable */
/* @flow */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';
import Helmet from 'react-helmet';

import * as action from './action';
import type { ReposLang as ReposLangType, Dispatch, Reducer } from '../../../types';
import Loading from '../Loading';

import './styles.scss';

type Props = {
	reposlang: ReposLangType,
	fetchReposByLang: () => void,
}

function SubComponent(props) {
	const languages = ['All', 'JavaScript', 'Ruby', 'Java', 'CSS', 'Python', 'C++'];
	return (
		<ul className="SubComponent">
			{languages.map((lang) => {
				return (
					<li
						style={lang === props.selectedLanguage ? { color: '#d0021b' } : null}
						onClick={props.onSelect.bind(null, lang)}
						key={lang}>
						{lang}
					</li>
				);
			})}
		</ul>
	);
}

function RepoGrid (props) {
	return (
		<ul className="RepoGrid">
			{props.repos.map(function (repo, index) {
				return (
					<li key={repo.name} className="RepoItem">
						<div className="popular_rank">#{index + 1}</div>
						<ul className="space_list_items">
							<li>
								<img
									className="avatar"
									src={repo.owner.avatar_url}
									alt={`Avatar for ${repo.owner.login}`}
									/>
							</li>
							<li><a href={repo.html_url} target="_blank">{repo.name}</a></li>
							<li>@{repo.owner.login}</li>
							<li>{repo.stargazers_count} stars</li>
						</ul>
					</li>
				);
			})}
		</ul>
	);
}

export class ReposLang extends PureComponent {
	props: Props;
	constructor(props) {
		super(props);
		this.state = {
			selectedLanguage: 'All',
		};
		this.changeLanguage = this.changeLanguage.bind(this);
	}
	static defaultProps: {
		reposlang: {
			readyStatus: "REPOS_NEW_INVALID",
			repos: null,
			fetching: false,
			fetched: false,
		},
		fetchReposByLang: () => {},
	}
	componentDidMount() {
		this.props.fetchReposByLang(this.state.selectedLanguage);
	}
	changeLanguage(lang) {
		const currentState = this.state.selectedLanguage;
		if (currentState !== lang) {
			this.setState(function () {
				return {
					selectedLanguage: lang,
				};
			});
			this.props.fetchReposByLang(lang);
		}
	}
	render() {
		return (
			<div>
				<Helmet>
					<title>The perfect page</title>
					<meta name="description" content="repos sorted by programming language" />
				</Helmet>
				<h2>Popular Repos</h2>
				<SubComponent onSelect={this.changeLanguage} selectedLanguage={this.state.selectedLanguage} />
				{this.props.reposlang.fetching ? <Loading /> : ''}
				{!this.props.reposlang.repos.items ? '' : <RepoGrid repos={this.props.reposlang.repos.items} />}
			</div>
		);
	}
}

const connector: Connector<{}, Props> = connect(
	({ reposlang }: Reducer) => ({ reposlang }),
	(dispatch: Dispatch) => ({
		fetchReposByLang: (lang) => dispatch(action.fetchReposByLang(lang)),
	}),
);

export default connector(ReposLang);
