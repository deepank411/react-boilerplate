/* @flow */
/* eslint-disable */

import type {
	Dispatch,
	GetState,
	ThunkAction,
	Reducer,
} from '../../../types';

export const REPOS_NEW_INVALID = 'REPOS_NEW_INVALID';
export const REPOS_NEW_REQUESTING = 'REPOS_NEW_REQUESTING';
export const REPOS_NEW_FAILURE = 'REPOS_NEW_FAILURE';
export const REPOS_NEW_SUCCESS = 'REPOS_NEW_SUCCESS';

export const fetchReposNew = (axios: any, URL: string): ThunkAction => (dispatch: Dispatch) => {
	dispatch({ type: REPOS_NEW_REQUESTING });

	return axios.get(URL)
	.then((response) => {
		dispatch({ type: REPOS_NEW_SUCCESS, payload: response.data });
	})
	.catch((err) => {
		dispatch({ type: REPOS_NEW_FAILURE, payload: err });
	});
};

// Preventing double fetching data
/* istanbul ignore next */
const shouldFetchReposByLang = (state: Reducer): boolean => {
	// In development, we will allow action dispatching
	// or your reducer hot reloading won't updated on the view
	if (__DEV__) return true;

	const reposlang = state.reposlang;

	if (reposlang.readyStatus === REPOS_NEW_SUCCESS) return false; // Preventing double fetching data

	return true;
};

/* istanbul ignore next */
export const fetchReposByLang = (language: string = language): ThunkAction =>
(dispatch: Dispatch, getState: GetState, axios: any) => {
	const url = `https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories&page=1&per_page=50`;

	/* istanbul ignore next */
	if (shouldFetchReposByLang(getState())) {
		/* istanbul ignore next */
		return dispatch(fetchReposNew(axios, url));
	}
	/* istanbul ignore next */
	return null;
};
