/* @flow */

import _ from 'lodash';

import {
	REPOS_INVALID,
	REPOS_REQUESTING,
	REPOS_FAILURE,
	REPOS_SUCCESS,
} from './action';
import type { Popular, Action } from '../../../types';

type State = Popular;

const initialState = {
	readyStatus: REPOS_INVALID,
	err: null,
	list: [],
};

export default (state: State = initialState, action: Action): State => {
	switch (action.type) {
	case REPOS_REQUESTING:
		return _.assign({}, state, {
			readyStatus: REPOS_REQUESTING,
		});
	case REPOS_FAILURE:
		return _.assign({}, state, {
			readyStatus: REPOS_FAILURE,
			err: action.err,
		});
	case REPOS_SUCCESS:
		return _.assign({}, state, {
			readyStatus: REPOS_SUCCESS,
			info: action.data,
		});
	default:
		return state;
	}
};
