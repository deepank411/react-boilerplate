/* eslint-disable */

import React, { PureComponent } from 'react';

// import { connect } from 'react-redux';
// import type { Connector } from 'react-redux';
import Helmet from 'react-helmet';

export class Home extends PureComponent {

	render() {
		return (
			<div>
				<Helmet title="first desktop component" />
				<Helmet description="desktop home page" />
				<p>desktop home page</p>
			</div>
		);
	}
}

export default Home;
