/* @flow */
/* eslint-disable import/no-named-as-default */

import type { Dispatch } from './types';
import { fetchReposIfNeeded } from './containers/mobile/PopularRepos/action';
import { fetchReposByLang } from './containers/mobile/ReposLang/action';
import HomePage from './containers/mobile/Home';
import ReposLang from './containers/mobile/ReposLang';
import PopularRepos from './containers/mobile/PopularRepos';

export default [
	{
		path: '/',
		exact: true,
		component: HomePage,
	},
	{
		path: '/repos-lang',
		component: ReposLang,
		loadData: (dispatch: Dispatch) => Promise.all([
			dispatch(fetchReposByLang()),
		]),
	},
	{
		path: '/popular',
		component: PopularRepos,
		loadData: (dispatch: Dispatch) => Promise.all([
			dispatch(fetchReposIfNeeded()),
		]),
	},
];
